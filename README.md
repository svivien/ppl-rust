# PPL-Rust : Probabilistic Programming Language Rust

Library for probabilistic programming developed in Rust by Samuel VIVIEN for the Probabilistic Programming Language course of the MPRI :

[https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-40]()


## Requirements

- `cargo` to install the dependencies required by this project
- Rust >= 1.56.1

The rust dependencies are :
- `rand v0.8.5` to generate random numbers
- test-only dependance : `plotters v0.3.4` to draw a graph in one of the tests

## Testing

Run `cargo test` to launch the test from this library. See `src/test/*rs` files to see the tests that are run.

Rust >= 1.61 is required for the tests because of the `plotters` library msrv.

Some tests have a certain probability to fail due to their random semantic. However, they pass almost all the time.

## Documentation

Run `cargo doc --open` to compile and open the documentation of this code in your web browser.

## Usage

This crate was designed as a library as such you can use it in any rust project by giving to cargo the path to this library.

### How are probabilistic laws represented ?

The idea this code is based on is the representation of random variables as stream of values. I also wanted to have as much polymorphism as possible.

In order to achieve polymorphism I defined a trait (works a bit like type classes in Haskell) called `ProbLaw` that all structure that represent probabilistic laws implement. This trait was designed to look pretty much like the `Iterator` trait.

The most important method of this trait is `sample` that sample a random variable.

There are also 2 more traits that are implemented by some probability laws : 
- `ProbLawStats` if you can compute things like mean or standard deviation,
- `ProbEnumerable` if it is possible to compute a hash-map mapping the values to their probability.

### Small example

For example is one want to compute the probability law presented in the first question of [https://github.com/mpri-probprog/probprog-22-23/blob/main/td/td2.pdf]().

To do this you need to :
- first define 3 Bernoulli probabilistic laws `A`, `B` and `C`,
- then zip them together to define the probabilistic law `A x B x C`,
- then filter to keep only cases where one of the two first was a success
- then map it to compute the sum of the triplet
- lastly you can compute the probability table

```rust
let a = crate::bernoulli::Bernoulli::<usize>::new(0.5);
let b = crate::bernoulli::Bernoulli::<usize>::new(0.5);
let c = crate::bernoulli::Bernoulli::<usize>::new(0.5);

let distrib = a
    .zip(b)
    .zip(c)
    .filter(|((a, b), _)| *a == 1 || *b == 1)
    .map(|((a, b), c)| a + b + c);

let probabilities = distib.to_table();

println!("{:?}", distrib);
```

The main difference with BYO-PPL is that the user does not directly sample values before computation but only after. As such it is much easier to computed various statistics about the probability law. However, this requires the library to allow as many operations on probability laws as possible.

Operations defined include :
- mapping
- zipping two probability laws
- filter
- building a finite (or infinite) iterator
- building a random variable depending on a random variable (see `dependant` method)

### Implemented probabilistic laws

- Constant probability law
- Bernoulli
- Binomial
- Uniform on a range of integers or float
- Uniform on a multiset of values
- Normal law (implemented using Box–Muller transform)
- Custom build from a probability table

## Limits

### Rejection Sampling

Filtering is implemented by rejection sampling however this is implemented naively by sampling until finding a value that is accepted by the filter. As such program can diverge if values accepted by the filter are improbable to sample.

For example :

```rust
let rand_num = crate::uniform::Range::<usize> {
    min : 0,
    max : usize::MAX,
};

let is_zero = rand_num.filter(|i| *i == 0).sample;
```

This program does not diverge but will take in average $2^{64}$ samplings before terminating.

This examples also shows another limit of this library : ranges does not include the upper bound.
As such, it is not possible to define a uniform probability law on all $64$ bits integers.

### Enumeration

Computing the probability table of a random value is done by enumeration of all possible paths. As such this is highly inefficient for probability laws such as uniform on a large range of values. However, this method is exact !

### Hash-map for probability tables

Using a hash-map to represent probability tables has an important side effect : probability tables can sometimes not be computed because of this. The problem comes from the fact that the values are used as the keys of the hash-map. As such the output type of the probability law must implement `Hash` and `Eq`. However, this is not the case for the `f32` and `f64` type (they implement `PartialEq` but not `Eq`).
As such it is not possible to compute the probability table of the Bernoulli law on floats.

### Consequences of polymorphism

The polymorphism made the implementation of binomial law really inefficient because all the values are computed as big sum rather than just being the number of success.

However, this allows the user to use this probability law for any type he would want to define (see `src/tests/special_bernoulli.rs`).
