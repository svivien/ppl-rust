use crate::{ProbEnumerable, ProbLaw, ProbLawStats};
use rand::prelude::*;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::Add;

#[derive(Debug, Clone)]
/// Structure representing a table value-probability
pub struct EnumerationTable<T: Hash + Eq> {
    table: HashMap<T, f64>,
}

impl<T: Hash + Eq> EnumerationTable<T> {
    /// Create a new enumeration table from a hashmap
    pub fn from_table(table: HashMap<T, f64>) -> Self {
        Self { table }
    }

    /// Create a probability table for a constant probability law
    pub fn from_const(t: T) -> Self {
        Self {
            table: [(t, 1.)].into_iter().collect(),
        }
    }

    /// Get size of support (will count some values of probability 0)
    pub fn len(&self) -> usize {
        self.table.len()
    }

    /// Returns the support (may contains values of probability 0)
    pub fn keys(&self) -> std::collections::hash_map::Keys<'_, T, f64> {
        self.table.keys()
    }

    /// Returns an iterator on the probability table
    pub fn iter(&self) -> std::collections::hash_map::Iter<'_, T, f64> {
        self.table.iter()
    }

    /// Get the probability of obtaining a value `k`.
    pub fn get(&self, k: &T) -> f64 {
        match self.table.get(k) {
            None => 0.,
            Some(p) => *p,
        }
    }

    /// Normalize probability law such that the sum of probability is 1.
    pub fn normalized(mut self) -> Self {
        let mut sum = 0.;
        for (_, v) in self.table.iter() {
            sum += *v
        }
        for (_, v) in self.table.iter_mut() {
            *v /= sum
        }
        self
    }
}

impl<T: Hash + Eq + Clone> EnumerationTable<T> {
    /// Return most probable value and it's probability
    pub fn max_prob(&self) -> (T, f64) {
        let mut iter = self.table.iter();
        let (mut best_val, best_s) = iter.next().unwrap();
        let mut best_s = *best_s;
        for (val, s) in iter {
            if *s > best_s {
                best_s = *s;
                best_val = val;
            }
        }
        (best_val.clone(), best_s)
    }
}

impl<T: Hash + Eq> IntoIterator for EnumerationTable<T> {
    type Item = (T, f64);

    type IntoIter = std::collections::hash_map::IntoIter<T, f64>;

    fn into_iter(self) -> Self::IntoIter {
        self.table.into_iter()
    }
}

impl<T: Hash + Eq + Clone> EnumerationTable<T> {
    /// Create a probability law from a probability table
    pub fn to_law(self) -> TableDistrib<T> {
        TableDistrib { table: self }
    }
}

impl<U: Hash + Eq + Clone + Add<V, Output = W>, V: Hash + Eq + Clone, W: Hash + Eq>
    Add<EnumerationTable<V>> for EnumerationTable<U>
{
    type Output = EnumerationTable<W>;

    fn add(self, other: EnumerationTable<V>) -> EnumerationTable<W> {
        let mut table = HashMap::new();
        for (t1, f1) in self.table {
            for (t2, f2) in &other.table {
                let v = table.entry(t1.clone() + t2.clone()).or_insert(0.);
                *v += f1 * *f2;
            }
        }
        EnumerationTable { table }
    }
}

/// Structure representing a probability law build from a probability table
/// It is assumed that the sum of the probability is 0
#[derive(Debug, Clone)]
pub struct TableDistrib<T: Hash + Eq> {
    table: EnumerationTable<T>,
}

impl<T: Hash + Eq + Clone> ProbLaw for TableDistrib<T> {
    type Item = T;

    fn sample(&self) -> Self::Item {
        let mut r: f64 = random();
        for (k, v) in self.table.iter() {
            r -= *v;
            if r <= 0. {
                return k.clone();
            }
        }
        panic!("Was not able to sample from table distibution")
    }

    fn try_sample(&self) -> Option<Self::Item> {
        let mut r: f64 = random();
        for (k, v) in self.table.iter() {
            r -= *v;
            if r <= 0. {
                return Some(k.clone());
            }
        }
        None
    }
}

impl<T: Hash + Eq + Clone + Into<f64>> ProbLawStats for TableDistrib<T> {
    fn mean(&self) -> f64 {
        let mut s = 0.;
        for (v, p) in self.table.iter() {
            s += v.clone().into() * *p;
        }
        s
    }

    fn var(&self) -> f64 {
        let mean = self.mean();
        let mut s = 0.;
        for (v, p) in self.table.iter() {
            s += (v.clone().into() - mean).powi(2) * *p;
        }
        s
    }
}

impl<T: Hash + Eq + Clone> ProbEnumerable<T> for TableDistrib<T> {
    fn to_table(&self) -> EnumerationTable<T> {
        self.table.clone()
    }
}
