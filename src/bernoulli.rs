use crate::{table::EnumerationTable, ProbEnumerable, ProbLaw, ProbLawStats};
use rand::prelude::*;
use std::collections::HashMap;
use std::hash::Hash;
use std::ops::Add;

/// Constant probability law
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Constant<T: Clone> {
    /// Value returned by the probability law
    pub v: T,
}

impl<T: Clone> ProbLaw for Constant<T> {
    type Item = T;

    fn sample(&self) -> Self::Item {
        self.v.clone()
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some(self.sample())
    }
}

impl<T: Clone + Into<f64>> ProbLawStats for Constant<T> {
    fn mean(&self) -> f64 {
        self.v.clone().into()
    }

    fn var(&self) -> f64 {
        0.
    }
}

impl<T: Clone + Hash + Eq> ProbEnumerable<T> for Constant<T> {
    fn to_table(&self) -> EnumerationTable<T> {
        EnumerationTable::from_const(self.v.clone())
    }

    fn sample_n(&self, n: usize) -> std::collections::HashMap<T, usize> {
        [(self.v.clone(), n)].into_iter().collect()
    }
}

/// Bernoulli probability law
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Bernoulli<T: Clone> {
    /// Value obtained if failed
    pub v1: T,

    /// Valued obtained if success
    pub v2: T,

    /// probability of obtaining v2
    pub p: f64,
}

macro_rules! def_bernoulli {
    ($id:ident, $u:expr, $v:expr) => {
        impl Bernoulli<$id> {
            /// Returns default bernoulli for this type
            pub fn new(p: f64) -> Self {
                Self { v1: $u, v2: $v, p }
            }
        }
    };
}

def_bernoulli!(i32, 0, 1);
def_bernoulli!(u8, 0, 1);
def_bernoulli!(i8, 0, 1);
def_bernoulli!(u16, 0, 1);
def_bernoulli!(i16, 0, 1);
def_bernoulli!(u32, 0, 1);
def_bernoulli!(u64, 0, 1);
def_bernoulli!(i64, 0, 1);
def_bernoulli!(usize, 0, 1);
def_bernoulli!(isize, 0, 1);
def_bernoulli!(bool, false, true);
def_bernoulli!(f32, 0., 1.);
def_bernoulli!(f64, 0., 1.);

impl<T: Clone> ProbLaw for Bernoulli<T> {
    type Item = T;

    fn sample(&self) -> Self::Item {
        if random::<f64>() > self.p {
            self.v1.clone()
        } else {
            self.v2.clone()
        }
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some(self.sample())
    }
}

impl<T: Clone + Into<f64>> ProbLawStats for Bernoulli<T> {
    fn mean(&self) -> f64 {
        self.v1.clone().into() * (1. - self.p) + self.v2.clone().into() * self.p
    }

    fn var(&self) -> f64 {
        let mean = self.mean();
        (self.v1.clone().into() - mean).powi(2) * (1. - self.p)
            + (self.v2.clone().into() - mean).powi(2) * self.p
    }
}

impl<T: Clone + Hash + Eq> ProbEnumerable<T> for Bernoulli<T> {
    fn to_table(&self) -> EnumerationTable<T> {
        EnumerationTable::from_table(
            [(self.v1.clone(), 1. - self.p), (self.v2.clone(), self.p)]
                .into_iter()
                .collect(),
        )
    }

    fn sample_n(&self, n: usize) -> HashMap<T, usize> {
        let mut map = HashMap::new();
        for _ in 0..n {
            *map.entry(self.sample()).or_insert(0) += 1;
        }
        map
    }
}

/// Binomail law, however this is really poorly implemented for default arithmetic types
/// We also assume addition to by associative and commutative
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Binomial<T: Add<T, Output = T> + Clone> {
    /// Value obtained if failed
    pub v1: T,

    /// Value obtained if success
    pub v2: T,

    /// probability of obtaining v2
    pub p: f64,

    /// Number of trials
    pub n: usize,
}

macro_rules! def_binomial {
    ($id:ident) => {
        impl Binomial<$id> {
            /// Returns default binomial for this type
            pub fn new(n: usize, p: f64) -> Self {
                Self { v1: 0, v2: 1, p, n }
            }
        }
    };
}

def_binomial!(i32);
def_binomial!(u8);
def_binomial!(i8);
def_binomial!(u16);
def_binomial!(i16);
def_binomial!(u32);
def_binomial!(u64);
def_binomial!(i64);
def_binomial!(usize);
def_binomial!(isize);

impl<T: Add<T, Output = T> + Clone> ProbLaw for Binomial<T> {
    type Item = T;

    fn sample(&self) -> Self::Item {
        assert!(self.n > 0);
        let mut val = if random::<f64>() > self.p {
            self.v1.clone()
        } else {
            self.v2.clone()
        };
        for _ in 1..self.n {
            val = val
                + if random::<f64>() > self.p {
                    self.v1.clone()
                } else {
                    self.v2.clone()
                }
        }
        val
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some(self.sample())
    }
}

impl<T: Clone + Into<f64> + Add<T, Output = T>> ProbLawStats for Binomial<T> {
    fn mean(&self) -> f64 {
        self.n as f64 * (self.v1.clone().into() * (1. - self.p) + self.v2.clone().into() * self.p)
    }

    fn var(&self) -> f64 {
        let mean = self.v1.clone().into() * (1. - self.p) + self.v2.clone().into() * self.p;
        let var1 = (self.v1.clone().into() - mean).powi(2) * (1. - self.p)
            + (self.v2.clone().into() - mean).powi(2) * self.p;
        self.n as f64 * var1
    }
}

fn coef_bin(n: usize, i: usize) -> f64 {
    if i == 0 {
        1.
    } else {
        (n as f64) / (i as f64) * coef_bin(n - 1, i - 1)
    }
}

impl<T: Clone + Hash + Eq + Add<T, Output = T>> ProbEnumerable<T> for Binomial<T> {
    fn to_table(&self) -> EnumerationTable<T> {
        assert!(self.n > 0);
        let mut table = HashMap::new();
        let mut v = self.v2.clone();
        for _ in 1..self.n {
            v = v + self.v2.clone()
        }
        table.insert(v, self.p.powi(self.n as i32));
        for i in 1..=self.n {
            let mut v = self.v1.clone();
            for _ in 1..i {
                v = v + self.v1.clone();
            }
            for _ in i..self.n {
                v = v + self.v2.clone();
            }
            table.insert(
                v,
                coef_bin(self.n, i)
                    * (1. - self.p).powi(i as i32)
                    * self.p.powi((self.n - i) as i32),
            );
        }

        EnumerationTable::from_table(table)
    }

    fn sample_n(&self, n: usize) -> HashMap<T, usize> {
        let mut map = HashMap::new();
        for _ in 0..n {
            *map.entry(self.sample()).or_insert(0) += 1;
        }
        map
    }
}
