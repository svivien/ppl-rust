use crate::table::EnumerationTable;
use crate::{ProbEnumerable, ProbLaw, ProbLawStats};
use rand::prelude::*;
use std::collections::HashMap;
use std::hash::Hash;

/// Random probability law on interval [min, max)
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Range<T> {
    /// Min value (included)
    pub min: T,
    /// Max value (not included)
    pub max: T,
}

macro_rules! impl_ProbLaw_int {
    ($id:ident) => {
        impl ProbLaw for Range<$id> {
            type Item = $id;

            fn sample(&self) -> Self::Item {
                let range = (self.max - self.min) as u128;
                (random::<u128>() % range) as $id + self.min
            }

            fn try_sample(&self) -> Option<Self::Item> {
                if self.max > self.min {
                    Some(self.sample())
                } else {
                    None
                }
            }
        }

        impl ProbLawStats for Range<$id> {
            fn mean(&self) -> f64 {
                (self.max as f64 + self.min as f64) / 2.
            }

            fn var(&self) -> f64 {
                let range = (self.max - self.min) as f64;
                (range * range - 1.) / 12.
            }
        }

        impl ProbEnumerable<$id> for Range<$id> {
            fn to_table(&self) -> EnumerationTable<$id> {
                let mut map = HashMap::new();
                let p = 1. / ((self.max - self.min) as f64);
                for i in self.min..self.max {
                    map.insert(i, p);
                }
                EnumerationTable::from_table(map)
            }
        }
    };
}

impl_ProbLaw_int!(i8);
impl_ProbLaw_int!(u8);
impl_ProbLaw_int!(i16);
impl_ProbLaw_int!(u16);
impl_ProbLaw_int!(i32);
impl_ProbLaw_int!(u32);
impl_ProbLaw_int!(i64);
impl_ProbLaw_int!(u64);
impl_ProbLaw_int!(isize);
impl_ProbLaw_int!(usize);

macro_rules! impl_ProbLaw_float {
    ($id:ident) => {
        impl ProbLaw for Range<$id> {
            type Item = $id;

            fn sample(&self) -> Self::Item {
                random::<$id>() * (self.max - self.min) + self.min
            }

            fn try_sample(&self) -> Option<Self::Item> {
                if self.max > self.min {
                    Some(self.sample())
                } else {
                    None
                }
            }
        }

        impl ProbLawStats for Range<$id> {
            fn mean(&self) -> f64 {
                (self.max + self.min) as f64 / 2.
            }

            fn var(&self) -> f64 {
                let i = (self.max - self.min) as f64;
                (i * i) / 12.
            }
        }
    };
}

impl_ProbLaw_float!(f32);
impl_ProbLaw_float!(f64);

/// Uniform probability law on a set of values represented as a vector
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Set<T: Clone> {
    /// Vector representing the set of values
    /// A value can be included multiple times
    pub s: Vec<T>,
}

impl<T: Clone> ProbLaw for Set<T> {
    type Item = T;

    fn sample(&self) -> Self::Item {
        self.s[random::<usize>() % self.s.len()].clone()
    }

    fn try_sample(&self) -> Option<Self::Item> {
        if self.s.is_empty() {
            None
        } else {
            Some(self.s[random::<usize>() % self.s.len()].clone())
        }
    }
}

impl<T: Eq + Clone + Hash> ProbEnumerable<T> for Set<T> {
    fn to_table(&self) -> crate::table::EnumerationTable<T> {
        let mut table = HashMap::new();
        let p = 1. / (self.s.len() as f64);
        for key in self.s.iter() {
            *table.entry(key.clone()).or_insert(0.) += p;
        }
        EnumerationTable::from_table(table)
    }
}

impl<T: Clone + Into<f64>> ProbLawStats for Set<T> {
    fn mean(&self) -> f64 {
        let mut sum = 0.;
        for el in &self.s {
            sum += el.clone().into();
        }
        sum / (self.s.len() as f64)
    }

    fn var(&self) -> f64 {
        let mean = self.mean();
        let mut sum = 0.;
        for el in &self.s {
            sum += (el.clone().into() - mean).powi(2)
        }
        sum / (self.s.len() as f64)
    }
}
