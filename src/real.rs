use crate::{ProbLaw, ProbLawStats};
use rand::prelude::*;

/// Normal probability law
/// This is implemented using the
#[derive(Debug, Clone, PartialEq)]
pub struct Normal {
    /// Mean
    pub mean: f64,
    /// Standard deviation
    pub std_dev: f64,
}

impl Normal {
    /// Normal law sampling with law of mean 0 and standard deviation 1
    /// using the Box–Muller transform
    fn normal_rand() -> f64 {
        let u1 = random::<f64>();
        let u2 = random::<f64>();
        (-2. * u1.ln()).sqrt() * (2. * std::f64::consts::PI * u2).cos()
    }
}

impl ProbLaw for Normal {
    type Item = f64;

    fn sample(&self) -> f64 {
        Self::normal_rand() * self.std_dev + self.mean
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some(self.sample())
    }
}

impl ProbLawStats for Normal {
    fn mean(&self) -> f64 {
        self.mean
    }

    fn std_dev(&self) -> f64 {
        self.std_dev
    }

    fn var(&self) -> f64 {
        self.std_dev * self.std_dev
    }
}
