//!
//! A small rust library to do a bit of probabilistic programming
//!
//! Author : Samuel VIVIEN
//! 2022-2023

#![warn(missing_docs)]

use std::{collections::HashMap, hash::Hash};

mod tests;

/// Default discrete probability laws
pub mod bernoulli;

/// Various structures to represent operations of probability laws (such as [`ops::ProbZip<A, B>`] and [`ops::ProbMap<T, F>`])
#[allow(missing_docs)]
pub mod ops;

/// Uniform probability laws on various supports
pub mod uniform;

/// Probability laws on "real" numbers [`real::Normal`]
pub mod real;

/// Probability tables and probability law build from such a table
pub mod table;

/// Trait representing a probability law
pub trait ProbLaw: Sized {
    /// Type of items returned by the probability law
    type Item;

    /// Try to sample an element (may return None if a filter fails)
    fn try_sample(&self) -> Option<Self::Item>;

    /// Sample an element (runs [`ProbLaw::try_sample`] until getting an element that passes all filters)
    fn sample(&self) -> Self::Item;

    /// Calls [`ProbLaw::try_sample`] n times and returns the amount of time it succeeded
    fn monte_carlo_proportion(&self, n: usize) -> usize {
        let mut t = 0;
        for _ in 0..n {
            if self.try_sample().is_some() {
                t += 1
            }
        }
        t
    }

    /// Calls [`ProbLaw::try_sample`] n times and returns the vector of the outputs
    fn monte_carlo_sampling(&self, n: usize) -> Vec<Self::Item> {
        let mut t = Vec::new();
        for _ in 0..n {
            if let Some(v) = self.try_sample() {
                t.push(v)
            }
        }
        t
    }

    /// Given a probability law X and a function f returns the probability law f(X)
    fn map<F>(self, f: F) -> ops::ProbMap<Self, F> {
        ops::ProbMap::new(self, f)
    }

    /// Given a probability law X and a function f : X::Item -> Y where Y is the type of a probabilty law
    /// creates a probability law of (X::Item, Y::Item)
    fn dependant<F>(self, f: F) -> ops::ProbDependant<Self, F> {
        ops::ProbDependant::new(self, f)
    }

    /// Given two probability laws A and B returns A x B
    fn zip<B>(self, other: B) -> ops::ProbZip<Self, B> {
        ops::ProbZip::new(self, other)
    }

    /// Filters a probability law
    fn filter<P>(self, p: P) -> ops::ProbFilter<Self, P>
    where
        P: Fn(&Self::Item) -> bool,
    {
        ops::ProbFilter::new(self, p)
    }

    /// Creates an iterator containing n elements from the probability law
    /// For example the following code creates a vector of `100` coin tossing result
    /// ````
    /// use crate::ppl_rust::ProbLaw;
    /// let coin = ppl_rust::bernoulli::Bernoulli::<i32>::new(0.5);
    /// let results : Vec<_> = coin.take(100).collect();
    fn take(self, n: usize) -> ops::ProbTake<Self> {
        ops::ProbTake::new(self, n)
    }

    /// Creates an unbounded iterator from the probability law
    fn repeat(self) -> ops::ProbRepeat<Self> {
        ops::ProbRepeat::new(self)
    }
}

/// Various methods that not all probability laws implement
/// All the probability laws implement this trait if sufficent
/// conditions on their type parameter is fullfiled (must implement [`Into<f64>`])
pub trait ProbLawStats {
    /// Compute the mean of the probability law
    fn mean(&self) -> f64;

    /// Compute the variation of the probability law
    fn var(&self) -> f64;

    /// Compute the standard deviation of the probability law
    fn std_dev(&self) -> f64 {
        self.var().sqrt()
    }
}

/// Trait stating that the probability table of the probability law can be computed
pub trait ProbEnumerable<T: Hash + Eq>: ProbLaw<Item = T> {
    /// Compute the probability table
    fn to_table(&self) -> table::EnumerationTable<T>;

    /// Sample n elements and stores them in a hashmap
    /// This consumes less memory than using [`ProbLaw::take`] comibined with [`Iterator::collect`]
    /// for probability law having a support smaller than the number of samples done
    fn sample_n(&self, n: usize) -> HashMap<T, usize> {
        let mut map = HashMap::new();
        for _ in 0..n {
            *map.entry(self.sample()).or_insert(0) += 1;
        }
        map
    }
}

/// Small utility macro to write your own probabilistic function
#[macro_export]
macro_rules! condition {
    ( $e:expr ) => {
        if !$e {
            return None;
        }
    };
}

/// Same as [`ProbLaw::monte_carlo_sampling`] but for a function `f: () -> Option<X>`
#[macro_export]
macro_rules! monte_carlo {
    ( $f:ident ) => {
        monte_carlo!($f, 1000)
    };
    ( $f:ident, $nb:expr ) => {{
        let mut vec = Vec::new();
        for _ in 0..$nb {
            match $f() {
                None => (),
                Some(el) => vec.push(el),
            }
        }
        vec
    }};
}

/// Same as [`ProbLaw::take`] combined with [`Iterator::collect`] but for a function `f: () -> Option<X>`
#[macro_export]
macro_rules! sample_n {
    ( $f:ident ) => {
        sample_n!($f, 1000)
    };
    ( $f:ident, $nb:expr ) => {{
        let mut vec = Vec::new();
        while vec.len() < $nb {
            if let Some(el) = $f() {
                vec.push(el)
            }
        }
        vec
    }};
}

#[macro_export]
macro_rules! select {
    ( $f:ident, $nb:expr, $selector:ident, $initial:expr) => {{
        let mut best = $initial;
        for _ in 0..$nb {
            match $f() {
                None => (),
                Some(el) => best = $selector(best, el),
            }
        }
        best
    }};
}
