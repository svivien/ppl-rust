use crate::{ProbEnumerable, ProbLaw};
use std::{collections::HashMap, hash::Hash};

#[derive(Debug, Copy, Clone)]
pub struct ProbMap<I, F> {
    iter: I,
    f: F,
}

impl<I, F> ProbMap<I, F> {
    pub fn new(iter: I, f: F) -> Self {
        ProbMap { iter, f }
    }
}

impl<II, I: ProbLaw<Item = II>, F, B> ProbLaw for ProbMap<I, F>
where
    F: Fn(II) -> B,
{
    type Item = B;

    fn sample(&self) -> Self::Item {
        (self.f)(self.iter.sample())
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some((self.f)(self.iter.try_sample()?))
    }
}

impl<II: Hash + Eq, I: ProbEnumerable<II> + ProbLaw<Item = II>, F, B: Hash + Eq> ProbEnumerable<B>
    for ProbMap<I, F>
where
    F: Fn(II) -> B,
{
    fn to_table(&self) -> crate::table::EnumerationTable<B> {
        let mut table = HashMap::new();
        for (k, v) in self.iter.to_table() {
            let k2 = (self.f)(k);
            *table.entry(k2).or_insert(0.) += v;
        }
        crate::table::EnumerationTable::from_table(table)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ProbZip<A, B> {
    a: A,
    b: B,
}

impl<A, B> ProbZip<A, B> {
    pub fn new(a: A, b: B) -> Self {
        ProbZip { a, b }
    }
}

impl<A: ProbLaw, B: ProbLaw> ProbLaw for ProbZip<A, B> {
    type Item = (<A as ProbLaw>::Item, <B as ProbLaw>::Item);

    fn sample(&self) -> Self::Item {
        (self.a.sample(), self.b.sample())
    }

    fn try_sample(&self) -> Option<Self::Item> {
        Some((self.a.try_sample()?, self.b.try_sample()?))
    }
}

impl<
        IA: Hash + Eq + Clone,
        IB: Hash + Eq + Clone,
        A: ProbEnumerable<IA> + ProbLaw<Item = IA>,
        B: ProbEnumerable<IB> + ProbLaw<Item = IB>,
    > ProbEnumerable<(IA, IB)> for ProbZip<A, B>
{
    fn to_table(&self) -> crate::table::EnumerationTable<(IA, IB)> {
        let mut table = HashMap::new();
        for (ka, va) in self.a.to_table() {
            for (kb, vb) in self.b.to_table() {
                table.insert((ka.clone(), kb), va * vb);
            }
        }
        crate::table::EnumerationTable::from_table(table)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ProbFilter<I, P> {
    iter: I,
    p: P,
}

impl<I, P> ProbFilter<I, P> {
    pub fn new(iter: I, p: P) -> Self {
        ProbFilter { iter, p }
    }
}

impl<I: ProbLaw, P> ProbLaw for ProbFilter<I, P>
where
    P: Fn(&<I as ProbLaw>::Item) -> bool,
{
    type Item = <I as ProbLaw>::Item;

    fn sample(&self) -> Self::Item {
        let mut item = self.iter.sample();
        while !(self.p)(&item) {
            item = self.iter.sample()
        }
        item
    }

    fn try_sample(&self) -> Option<Self::Item> {
        let item = self.iter.try_sample()?;
        if (self.p)(&item) {
            Some(item)
        } else {
            None
        }
    }
}

impl<II: Hash + Eq, I: ProbLaw<Item = II> + ProbEnumerable<II>, P> ProbEnumerable<II>
    for ProbFilter<I, P>
where
    P: Fn(&II) -> bool,
{
    fn to_table(&self) -> crate::table::EnumerationTable<II> {
        crate::table::EnumerationTable::from_table(
            self.iter
                .to_table()
                .into_iter()
                .filter(|(k, _)| (self.p)(k))
                .collect(),
        )
        .normalized()
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ProbDependant<I, F> {
    iter: I,
    f: F,
}

impl<I, F> ProbDependant<I, F> {
    pub fn new(iter: I, f: F) -> Self {
        Self { iter, f }
    }
}

impl<II, I: ProbLaw<Item = II>, F, B> ProbLaw for ProbDependant<I, F>
where
    F: Fn(&II) -> B,
    B: ProbLaw,
{
    type Item = (II, <B as ProbLaw>::Item);

    fn sample(&self) -> Self::Item {
        let s1 = self.iter.sample();
        let s2 = (self.f)(&s1).sample();
        (s1, s2)
    }

    fn try_sample(&self) -> Option<Self::Item> {
        let s1 = self.iter.try_sample()?;
        let s2 = (self.f)(&s1).try_sample()?;
        Some((s1, s2))
    }
}

impl<
        II: Hash + Eq + Clone,
        I: ProbLaw<Item = II> + ProbEnumerable<II>,
        F,
        B: ProbLaw<Item = BI>,
        BI: Hash + Eq,
    > ProbEnumerable<(II, BI)> for ProbDependant<I, F>
where
    F: Fn(&II) -> B,
    B: ProbEnumerable<BI>,
{
    fn to_table(&self) -> crate::table::EnumerationTable<(II, BI)> {
        let mut table = HashMap::new();
        for (k, p1) in self.iter.to_table() {
            for (bi, p2) in (self.f)(&k).to_table() {
                *table.entry((k.clone(), bi)).or_insert(0.) += p1 * p2;
            }
        }
        crate::table::EnumerationTable::from_table(table)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ProbTake<T> {
    prob_law: T,
    nb_left: usize,
}

impl<T> ProbTake<T> {
    pub fn new(prob_law: T, nb_left: usize) -> Self {
        Self { prob_law, nb_left }
    }
}

impl<T: ProbLaw> Iterator for ProbTake<T> {
    type Item = <T as ProbLaw>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.nb_left == 0 {
            None
        } else {
            self.nb_left -= 1;
            Some(self.prob_law.sample())
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ProbRepeat<T> {
    prob_law: T,
}

impl<T> ProbRepeat<T> {
    pub fn new(prob_law: T) -> Self {
        Self { prob_law }
    }
}

impl<T: ProbLaw> Iterator for ProbRepeat<T> {
    type Item = <T as ProbLaw>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.prob_law.sample())
    }
}
