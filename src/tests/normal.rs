use crate::{ProbLaw, ProbLawStats};

#[test]
fn test_normal() {
    let mean = 3.;
    let std_dev = 5.;
    let r = crate::real::Normal { mean, std_dev };

    let n = 10000;
    let mut v = Vec::with_capacity(n);
    for _ in 0..n {
        v.push(r.sample())
    }

    let s = crate::uniform::Set { s: v };

    println!("{} {}", s.mean(), s.std_dev());

    assert!((s.mean() - mean).abs() < 0.1);
    assert!((s.std_dev() - std_dev).abs() < 0.1);
}
