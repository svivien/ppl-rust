use crate::ProbLaw;

fn is_odd(n: &usize) -> bool {
    n % 2 == 1
}

fn gen_odd() -> Option<usize> {
    crate::bernoulli::Binomial::<usize> {
        v1: 0,
        v2: 1,
        p: 0.5,
        n: 100,
    }
    .filter(is_odd)
    .try_sample()
}

#[test]
fn test_gen_odd() {
    let nb_tests = 1000;
    let res = crate::monte_carlo!(gen_odd, nb_tests);
    assert!(res.len() <= nb_tests);
    assert!(res.len() <= (nb_tests * 55) / 100);
    assert!(res.len() >= (nb_tests * 45) / 100);
    for i in res {
        assert_eq!(i % 2, 1);
    }
}
