#[cfg(test)]
mod special_bernoulli;

#[cfg(test)]
mod monte_carlo;

#[cfg(test)]
mod graph;

#[cfg(test)]
mod td2;

#[cfg(test)]
mod normal;

#[cfg(test)]
mod discrete {
    use crate::{sample_n, ProbEnumerable, ProbLaw, ProbLawStats};

    #[test]
    fn test_bernoulli_law() {
        for _ in 0..10 {
            let p = crate::uniform::Range { min: 0., max: 1. }.sample();
            let law = crate::bernoulli::Bernoulli { v1: 0i32, v2: 1, p };
            let mut v = [0; 2];
            let nb_tests = 1000;
            for _ in 0..nb_tests {
                v[law.sample() as usize] += 1;
            }
            println!("{:?} {} {} {}", v, law.mean(), law.var(), p);
            let table = law.to_table();
            println!("{table:?}");
            assert!((table.get(&0) - (1. - p)).abs() < 1e-10);
            assert!((table.get(&1) - p).abs() < 1e-10);
            assert!((law.mean() - p).abs() < 1e-10);
            assert!((law.var() - p * (1. - p)).abs() < 1e-10);
            assert_eq!(v[0] + v[1], nb_tests);
            assert!(v[0] as f64 >= (nb_tests as f64) * (1. - p - 0.05)); // Ê[v[0]] = nb_tests * 50/100
            assert!(v[1] as f64 >= (nb_tests as f64) * (p - 0.05)); // Ê[v[1]] = nb_tests * 50/100
        }
    }

    #[test]
    fn test_bernoulli_sum_law() {
        let law = crate::bernoulli::Bernoulli::<usize> {
            v1: 0,
            v2: 1,
            p: 0.5,
        };
        let law = law.zip(law).map(|(x, y)| x + y);
        let mut v = [0; 3];
        let nb_tests = 1000;
        for _ in 0..nb_tests {
            v[law.sample()] += 1;
        }
        println!("{:?}", v);
        assert_eq!(v[0] + v[1] + v[2], nb_tests);
        assert!(v[0] >= (nb_tests * 21) / 100); // Ê[v[0]] = nb_tests * 25/100
        assert!(v[1] >= (nb_tests * 45) / 100); // Ê[v[1]] = nb_tests * 50/100
        assert!(v[2] >= (nb_tests * 21) / 100); // Ê[v[2]] = nb_tests * 25/100
    }

    #[test]
    fn test_bernoulli_monte_carlo() {
        let law = crate::bernoulli::Bernoulli::<usize> {
            v1: 0,
            v2: 1,
            p: 0.5,
        };
        let nb_tests = 1000;
        let res = law.sample_n(nb_tests);
        println!("{:?}", res);
        let v = [*res.get(&0).unwrap(), *res.get(&1).unwrap()];
        assert_eq!(v[0] + v[1], nb_tests);
        assert!(v[0] >= (nb_tests * 45) / 100); // Ê[v[0]] = nb_tests * 25/100
        assert!(v[1] >= (nb_tests * 45) / 100); // Ê[v[1]] = nb_tests * 50/100
    }

    // #[test]
    // fn test_small_enum() {
    //     let law = crate::bernoulli::Bernoulli::<i32> {
    //         v1: 0,
    //         v2: 1,
    //         p: 0.5,
    //     };
    //     let law = law.zip(law).map(|(x,y)| x + y);
    //     let table1 = law.to_table();
    //     let law2 = crate::bernoulli::Binomial::<i32> {
    //         v1: 0,
    //         v2: 1,
    //         p: 0.5,
    //         n: 2,
    //     };
    //     let table2 = law2.to_table();
    //     println!("{:?}", table1);
    //     println!("{:?}", table2);
    //     assert!((law.mean() - law2.mean()).abs() < 1e-10);
    //     println!("{} {}", law.var(), law2.var());
    //     assert!((law.var() - law2.var()).abs() < 1e-10);
    //     assert!((law.std_dev() - law2.std_dev()).abs() < 1e-10);
    //     assert_eq!(table1.len(), 3);
    //     assert_eq!(table2.len(), 3);
    //     for (k, v) in table1.into_iter() {
    //         let v2 = table2.get(&k);
    //         assert!((v - v2).abs() < 1e-10);
    //     }
    // }

    fn rand_0_100() -> Option<i32> {
        Some(crate::uniform::Range { min: 0, max: 100 }.sample())
    }

    #[test]
    fn construct_deconstruct() {
        let s = sample_n!(rand_0_100, 1000);
        let law1 = crate::uniform::Set { s };
        let table = law1.to_table();
        let law2 = table.to_law();

        assert!((law1.mean() - law2.mean()) < 1e-10);
        assert!((law1.var() - law2.var()) < 1e-10);
        assert!((law1.std_dev() - law2.std_dev()) < 1e-10);
    }
}
