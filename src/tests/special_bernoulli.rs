use std::iter::Sum;

use crate::ProbLaw;

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct MyNums {
    id: usize,
}

impl Sum<MyNums> for MyNums {
    fn sum<I>(i: I) -> Self
    where
        I: Iterator<Item = MyNums>,
    {
        let mut id = 0;
        for el in i {
            id += el.id
        }
        Self { id }
    }
}

#[test]
fn test_bernoulli_law() {
    let law = crate::bernoulli::Bernoulli {
        v1: MyNums { id: 0 },
        v2: MyNums { id: 1 },
        p: 0.5,
    };
    let mut v = [0; 2];
    let nb_tests = 1000;
    for _ in 0..nb_tests {
        v[law.sample().id] += 1;
    }
    println!("{:?}", v);
    assert_eq!(v[0] + v[1], nb_tests);
    assert!(v[0] >= (nb_tests * 45) / 100); // Ê[v[0]] = nb_tests * 50/100
    assert!(v[1] >= (nb_tests * 45) / 100); // Ê[v[1]] = nb_tests * 50/100
}
