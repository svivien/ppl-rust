use crate::ProbLaw;
use plotters::backend::BitMapBackend;
use plotters::chart::ChartBuilder;
use plotters::drawing::IntoDrawingArea;
use plotters::prelude::full_palette::{BLUE, WHITE};
use plotters::prelude::Circle;

fn rand_in_circle() -> Option<(f64, f64)> {
    let x = crate::uniform::Range::<f64> { min: 0., max: 1. };
    let y = crate::uniform::Range::<f64> { min: 0., max: 1. };
    x.zip(y).filter(|(x, y)| x * x + y * y < 1.).try_sample()
}

#[test]
fn draw() {
    let nb_points = 1000;
    let err = 0.05;
    let v = crate::monte_carlo!(rand_in_circle, nb_points);
    let expected = (nb_points as f64) * std::f64::consts::PI / 4.;
    assert!(v.len() as f64 >= expected * (1. - err));
    assert!(v.len() as f64 <= expected * (1. + err));

    let root_area = BitMapBackend::new("./test.png", (500, 500)).into_drawing_area();
    root_area.fill(&WHITE).unwrap();
    let mut ctx = ChartBuilder::on(&root_area)
        .caption("Portion of circle", ("sans-serif", 40.0))
        .x_label_area_size(40)
        .y_label_area_size(40)
        .build_cartesian_2d(0.0..1.0, 0.0..1.0)
        .unwrap();
    ctx.draw_series(v.iter().map(|point| Circle::new(*point, 4.0_f64, &BLUE)))
        .unwrap();
}
