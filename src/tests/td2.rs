use crate::{monte_carlo, ProbEnumerable, ProbLaw};

fn gen_number() -> Option<usize> {
    let a = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    let b = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    let c = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    Some(
        a.zip(b)
            .zip(c)
            .filter(|((a, b), _)| *a == 1 || *b == 1)
            .map(|((a, b), c)| a + b + c)
            .sample(),
    )
}

#[test]
fn exercise1() {
    let nb_tests = 1000;
    let s = monte_carlo!(gen_number, nb_tests);
    let new_law = crate::uniform::Set { s };
    let distrib = new_law.to_table();
    println!("{:?}", distrib);
    assert_eq!(distrib.len(), 3);
    assert!(distrib.get(&1) >= 0.31);
    assert!(distrib.get(&2) >= 0.46);
    assert!(distrib.get(&3) >= 0.14);
}

#[test]
fn exercise1b() {
    let a = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    let b = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    let c = crate::bernoulli::Bernoulli::<usize>::new(0.5);
    let distrib = a
        .zip(b)
        .zip(c)
        .filter(|((a, b), _)| *a == 1 || *b == 1)
        .map(|((a, b), c)| a + b + c)
        .to_table();
    println!("{:?}", distrib);

    assert_eq!(distrib.len(), 3);
    assert!((distrib.get(&1) - 1. / 3.).abs() < 1e-10);
    assert!((distrib.get(&2) - 1. / 2.).abs() < 1e-10);
    assert!((distrib.get(&3) - 1. / 6.).abs() < 1e-10);
}

#[test]
fn exercise2() {
    let n = 200;
    let success = 71;
    let prob_gen = crate::uniform::Range::<usize> { min: 0, max: n + 1 };
    let (estimated_prob, _) = prob_gen
        .dependant(|p: &usize| crate::bernoulli::Binomial::<usize>::new(n, *p as f64 / n as f64))
        .filter(|(_, q)| *q == success)
        .map(|(p, _)| p)
        .to_table()
        .max_prob()
        .clone();

    assert_eq!(estimated_prob, success)
}
